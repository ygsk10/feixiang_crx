const $ = jQuery
const bg = chrome.extension.getBackgroundPage()

if(!localStorage.getItem('uid') || !localStorage.getItem('sid')){
    openLoginPage()
}

function reset(){
    localStorage.clear()
    chrome.browserAction.setIcon({path:"../assets/vpn_icon_unlogin.png"})
    // openLoginPage()
}

function openLoginPage(){
    chrome.tabs.query({
        url: bg.FxVpn.constants.pageHost + '/login',
        currentWindow: true
    },function(tabs){
        if(tabs.length > 0){
            const tab = tabs[0]
            chrome.tabs.update(tab.id,{active: true})
        }
        else{
            chrome.tabs.create({url: bg.FxVpn.constants.pageHost + '/login'})
        }
    })
    window.close()
}

// 获取节点列表
function handleNodesList(result) {
    if(result && result.code === 200){
        const list = result.data
        let htmlArr = []
        if(list && list.length > 0){
            list.forEach(item => {
                htmlArr.push('<li class="'+(item.id === localStorage.getItem('nid') ? "selected" : "") +'" nid="'+item.id+'" areaAlias="'+item.area_alias+'" server="'+item.server+'" port="'+item.server_port+'" method="'+item.method+'" password="'+item.password+'">' +
                '<span class="icon panel"></span>' + 
                '<span>'+item.area_alias+'</span>' +
                '</li>'
                )
            })
            $('.node-list-panel ul').html(htmlArr.join(''))

            // 第一次打开面板默认登录第一个节点
            let timeout = setTimeout(function(){
                // 默认连接第一个节点
                if(!localStorage.getItem('showFirstConnect')){
                    $('.node-list-panel ul li:first-child').click();
                    localStorage.setItem('showFirstConnect',true)
                }
                clearTimeout(timeout)
            },500)
           
        }
        else{
            $('.node-list-panel ul').html('')
        }
    }
}

// 获取登录用户信息
function handlesSession(result){
    if(result && result.code === 200){
        $('#userName').html(result.data.userid && result.data.userid.replace(/^(\d{3})\d{4}(\d+)/,"$1****$2"))
        $('#packageName').html(result.data.good_name)
        $('#companyName').html(result.data.company_name)
        $('#userGroupName').html(result.data.strategy_name)

        bg.FxVpn.constants.setCurrentCompanyId(result.data.company_id)
        bg.FxVpn.constants.setCurrentBwLimit(result.data.band_width_limit)
        localStorage.setItem('phone', result.data.userid)

        if(result.data.good_expire_date <= 1){
            $('#pkgTips').css('visibility','unset')
        }
        else{
            $('#pkgTips').css('visibility','hidden')
        }
    }
    else {
        reset()
    }
}

function logout(){
    bg.FxVpn.common.logoutAjax(function(){
        reset()
        let timeout = setTimeout(function(){
            window.close()
            clearTimeout(timeout)
        },200)
    })
}

function triggerNodePanel(dom){
    if(!dom.style.display || dom.style.display === 'none'){
        dom.style.display = 'block'
    }
    else{
        dom.style.display = 'none'
    }
}

function handleConnectSuccess(nodeInfo){
    //dom 
    $('#unselectTips').hide()
    $('#selectingTips').hide()
    $('#currentNode').show()
    $('#currentNodeName').html(nodeInfo.areaAlias)

    // localStorage
    localStorage.setItem('sslocal-running', true)
    localStorage.setItem('nid',nodeInfo.nid)
    localStorage.setItem('areaAlias',nodeInfo.areaAlias)

    // proxy
    bg.FxVpn.proxy.switch('on')
}

function handleConnectFailure(){
    // dom
    $('#unselectTips').show()
    $('#selectingTips').hide()
    $('#currentNode').hide()
    $('#currentNodeName').html('')
    $('.node-list-panel ul li.selected').removeClass('selected')
    

    // localStorage
    localStorage.removeItem('sslocal-running')
    localStorage.removeItem('nid')
    localStorage.removeItem('areaAlias')

    // proxy
    bg.FxVpn.proxy.switch('off')
}

// 禁止右键
function stop(){
    return false;
}
document.oncontextmenu=stop;

$(() => {
    $connectInfo = document.getElementsByClassName('connect-info')[0]
    $currentNode = $connectInfo.getElementsByClassName('node')[0]
    $nodeListPanel = $connectInfo.getElementsByClassName('node-list-panel')[0]
    $nodeSelectedIcon = $currentNode.getElementsByClassName('select-icon')[0]
    $unselectBtn = $connectInfo.getElementsByClassName('unselect')[0]
    
    const $panelDom = $('.node-list-panel') 

    $unselectBtn.addEventListener('click',triggerNodePanel.bind(this,$nodeListPanel))
    $currentNode.addEventListener('click',triggerNodePanel.bind(this,$nodeListPanel))

    bg.FxVpn.link.getNodes(handleNodesList)
    bg.FxVpn.link.getSession(handlesSession)

    if(localStorage.getItem('nid')){
        handleConnectSuccess({
            nid: localStorage.getItem('nid'),
            areaAlias: localStorage.getItem('areaAlias'),
        })
    }

    // 节点点击
    $('.node-list-panel ul').on('click', 'li',async function(){
        $panelDom.hide()
        $('#unselectTips').hide()
        $('#selectingTips').show()
        $('.node-list-panel ul li.selected').removeClass('selected')
        $(this).addClass('selected')
        const serverAddr = $(this).attr('server')
        const serverPort = $(this).attr('port')
        const method = $(this).attr('method')
        const password = $(this).attr('password')
        const areaAlias = $(this).attr('areaAlias')
        const nid = $(this).attr('nid')
        
        var params = {
            server_addr: serverAddr,
            server_port: serverPort && parseInt(serverPort),
            method,
            password,
            local_addr: bg.FxVpn.sslocal.config.server,
            local_port: bg.FxVpn.sslocal.config.socks5Port,
            uid: parseInt(localStorage.getItem('uid')),
            company_id: bg.FxVpn.constants.currentCompanyId ,
            bw_limit: bg.FxVpn.constants.currentBwLimit ,
            url: bg.FxVpn.constants.apiHost + '/enterprise/v1/logout',
            type: 'PC',
            sid: localStorage.getItem('sid'),
            unique_identification: bg.FxVpn.constants.browserVersion,
            intranet_ip: localStorage.getItem('intarnetIp')
        }
        
        let flag = false
        const { check, connect, disconnect } = bg.FxVpn.sslocal
        for(let i =0; i < 3; i++){
            if(!flag){
                let running = await check()
                if(running){
                    let stopFlag = await disconnect()
                    if(stopFlag){
                        flag = await connect(params)
                    }
                }
                else{
                    flag = await connect(params)
                }
            }
            else{
                handleConnectSuccess({
                    areaAlias,
                    nid
                })
                break    
            }
        }

        // 如果flag仍为false，需要关闭代理
        if(!flag) {
            handleConnectFailure()
        }
    })
    
    // 退出登录
    $('#logout').on('click', async () => {
        logout()
        for(let i = 0; i< 3 ;i++){
            let flag = await bg.FxVpn.sslocal.disconnect('logout') // 断开sslocal
            if(flag){
                break
            }
        }
        bg.FxVpn.proxy.switch('off')
    })

    $(document).on('mouseup', e => {
        if(!$panelDom.is(e.target) && $panelDom.has(e.target).length === 0){
            $panelDom.hide()
        }
    })
})
