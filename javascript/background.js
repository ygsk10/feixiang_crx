const $ = jQuery

var FxVpn = {
    /**
     * 常量管理
     */
    constants:{
        currentCompanyId : '',
        currentBwLimit : '',
        browserVersion: '1.0.0.0',
        apiHost: 'https://ft-control.ifeixiang.com',
        dataReportUrl: 'https://data.ifeixiang.com/v1/o_app_log',
        pageHost: 'https://wos.ifeixiang.com',
        // apiHost: 'http://test-ft-control.yjcx-tech.com',
        // dataReportUrl: 'https://test-data.yjcx-tech.com/v1/o_app_log',
        // pageHost: 'https://wostest.ifeixiang.com',
        pacUrl:'http://static.yjcx-tech.com/pac/pac.txt',
        setCurrentCompanyId: function(companyId){
            this.currentCompanyId = companyId
        },
        setCurrentBwLimit: function(bwLimit){
            this.currentBwLimit = bwLimit
        }
    },
    common:{
        loginStatusWatching:function(tabId,url){
            if((!localStorage.getItem('sid') || 
                !localStorage.getItem('uid')) && 
                !/^https?:\/\/(.*?)\.ifeixiang.com(:\d+)?(\/.*)?$/.test(decodeURIComponent(url)) && 
                !/^https?:\/\/(.*?)\.edu.cn(:\d+)?(\/.*)?$/.test(decodeURIComponent(url)) && 
                ! url.startsWith('chrome://') &&
                ! url.startsWith('about://') &&
                tabId !== localStorage.getItem('firstTab')){
                chrome.tabs.update(tabId, {url: FxVpn.constants.pageHost + '/login'})
            }
            else if(tabId === localStorage.getItem('firstTab')){
                localStorage.removeItem('firstTab')
            }
        },
        logoutAjax:(callback = null) => {
            if(!localStorage.getItem('sid') || !localStorage.getItem('intarnetIp')){
                return
            }
            $.ajax({
                url: FxVpn.constants.apiHost + '/enterprise/v1/logout',
                cache: false,
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    type: 'PC',
                    sid: localStorage.getItem('sid'),
                    unique_identification: FxVpn.constants.browserVersion,
                    intranet_ip: localStorage.getItem('intarnetIp')
                }),
                success: async function(result){
                    callback && callback()
                    // 断开sslocal连接
                    let res = await FxVpn.sslocal.check()
                    if(res){
                        FxVpn.sslocal.disconnect()
                    }
                    // 清理页面localStorage
                    chrome.tabs.query({active: true, currentWindow: true}, function(tabs)
                    {
                        chrome.tabs.sendMessage(tabs[0].id, {'type':'FROM_BACKGROUND_LOGOUT'}, function(response)
                        {
                            // if(callback) callback(response);
                        });
                    });
                }
            })
        }
    },
    /**
     *  sslocal 进程管理
     */
    sslocal:{
        config: {
            server: '127.0.0.1',
            // port: 10888,
            // socks5Port: 10080
            port: 10889,
            socks5Port: 10081
        },
        // 初始化sslocal，浏览器启动加载sslocal.exe
        init: async function(){
            let result = await FxVpn.sslocal.check()
            if(result !== 'quit'){
                await FxVpn.sslocal.quit()
            }
            chrome.runtime.initSSlocal()
        },
        connect: function(params){
            return new Promise((resolve,reject)=>{
                $.ajax({
                    url: `http://127.0.0.1:${FxVpn.sslocal.config.port}/start`,
                    cache: false,
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(params),
                    success: function(result){
                        if(result && result.code === 200){
                            resolve(true)
                        }
                        else{
                            resolve(false)
                        }
                    },
                    error: function(error){
                        resolve(false)
                    }
                })
            })
        },
        disconnect: (mode = 'switch') => {
            return new Promise((resolve,reject) =>{
                $.ajax({
                    url: `http://127.0.0.1:${FxVpn.sslocal.config.port}/stop`,
                    cache: false,
                    type: 'POST',
                    data: {
                        sid: mode === 'switch' ? "" : localStorage.getItem('sid')
                    },
                    success: function(result){
                        if(result && result.code === 200 || result.code === 201){
                            resolve(true)
                        }
                        else{
                            resolve(false)
                        }
                    },
                    error: function(error){
                        resolve(false)
                    }
                })
            })
        },
        check: function(){
            return new Promise((resolve,reject)=>{
                $.ajax({
                    url: `http://127.0.0.1:${FxVpn.sslocal.config.port}/status`,
                    cache: false,
                    type: 'POST',
                    data: {},
                    success: function(result){
                        if(result && result.code === 200){
                            if(result.data.running){
                                resolve(true)
                            }
                            else{
                                resolve(false)
                            }
                        }
                        else{
                            resolve(false)
                        }
                    },
                    error: function(error){
                        resolve('quit')
                    }
                })
            })
        },
        quit:function(data = {}){
            return new Promise((resolve,reject)=>{
                $.ajax({
                    url: `http://127.0.0.1:${FxVpn.sslocal.config.port}/kill`,
                    type: 'POST',
                    data,
                    success: function(result){
                        if(result && result.code === 200){
                            if(result.data.running){
                                resolve(true)
                            }
                            else{
                                resolve(false)
                            }
                        }
                        else{
                            resolve(false)
                        }
                    },
                    error: function(error){
                        resolve(false)
                    }
                })
            })
        },
        getLocalInfo: function() {
            return new Promise((resolve,reject) => {
                $.ajax({
                    url: `http://127.0.0.1:${FxVpn.sslocal.config.port}/info`,
                    async: false,
                    cache: false,
                    type: 'POST',
                    contentType: 'application/json',
                    data: {},
                    success: function(result){
                        if(result && result.code === 200){
                            resolve(result.data)
                        }
                        else{
                            resolve(false)
                        }
                    },
                    error: function(error){
                        resolve(false)
                    }
                })
            })
        }
    },
    /**
     *  代理功能
     */
    proxy:{
        mode: 'off',
        pacData: null,
        init:function(){
            var dataurl = FxVpn.constants.pacUrl;
            var xhr = new XMLHttpRequest();
            xhr.open('GET', dataurl, true);
            xhr.responseType = 'text';
            xhr.send();
            xhr.onload = function(result){
                if(result.target.status === 200){
                    FxVpn.proxy.pacData = result.target.responseText
                }
            }
        },
        getConfig:function(){
            if(this.mode === 'on'){
                if(FxVpn.proxy.pacData){
                    console.log('local pac')
                    return {
                        mode: "pac_script",
                        pacScript:{
                            data: FxVpn.proxy.pacData
                        }
                    }
                }
                else{
                    return {
                        mode: "pac_script",
                        pacScript:{
                            url: FxVpn.constants.pacUrl,
                            mandatory: true
                        }
                    }
                }
            }
            else if(this.mode === 'off'){
                return {
                    mode: "pac_script",
                    pacScript:{
                        data: "var FindProxyForUrl = function(url, host){return 'DIRECT';}"
                    }
                }
            }
        },
        switch: function(mode){
            this.mode = mode
            chrome.proxy.settings.set({value: this.getConfig.call(this), scope: 'regular'},function() {})
        }
    },
    listeners:{
        // 与 webpage 通信
        messageListener:function(){
            chrome.runtime.onMessageExternal.addListener(async (request, sender, sendResponse) => {
                if(!sender || 
                    (!/^https?:\/\/(.*?)\.ifeixiang.com(:\d+)?(\/.*)?$/.test(decodeURIComponent(sender.origin)) && 
                    !/^https?:\/\/(.*?)\.edu.cn(:\d+)?(\/.*)?$/.test(decodeURIComponent(sender.origin)))){
                    sendResponse({type: 'FROM_BROWSER', code: 500, msg: 'error page host'});
                    return true
                }
    
                // sendResponse返回响应
                if (request && request.type == 'FROM_PAGE_SET_PROFILES' && request.sid) {
                    sendResponse({type: 'FROM_BROWSER', code: 0, msg: 'success'});
                    localStorage.setItem("uid",request.uid)
                    localStorage.setItem("sid",request.sid)
                    if(!request.uid || !request.sid){
                        chrome.browserAction.setIcon({path:"../assets/vpn_icon_unlogin.png"})
                    }
                    else{
                        chrome.browserAction.setIcon({path:"../assets/vpn_icon_login_70.png"})

                        // chrome.browserAction.setPopup({popup: 'vpn_panel.html'})
                        // 第一次登录自动连接节点
                        FxVpn.link.getSession(function(result){
                            if(result && result.code === 200){
                                FxVpn.link.getNodes(async function(data){
                                    if(data && data.code === 200){
                                        let list = data.data
                                        if(list && list.length > 0){
                                            let nodeInfo = list[0]
                                            let params = {
                                                server_addr: nodeInfo.server,
                                                server_port: parseInt(nodeInfo.server_port),
                                                method: nodeInfo.method,
                                                password: nodeInfo.password,
                                                local_addr: FxVpn.sslocal.config.server,
                                                local_port: FxVpn.sslocal.config.socks5Port,
                                                uid: parseInt(localStorage.getItem('uid')),
                                                company_id: result.data.company_id ,
                                                bw_limit: result.data.band_width_limit ,
                                            }
                                            
                                            let flag = false
                                            const { check, connect, disconnect } = FxVpn.sslocal
                                            for(let i =0; i < 3; i++){
                                                if(!flag){
                                                    let running = await check()
                                                    if(running){
                                                        let stopFlag = await disconnect()
                                                        if(stopFlag){
                                                            flag = await connect(params)
                                                        }
                                                    }
                                                    else{
                                                        flag = await connect(params)
                                                    }
                                                }
                                                else{
                                                    FxVpn.proxy.switch('on')
                                                    break    
                                                }
                                            }
                                        }
                                        
                                    }
                                })
                            }
                        })
                    }
                    return true
                }
                else if(request && request.type == 'FROM_PAGE_GET_INFO'){
                    let data = await FxVpn.sslocal.getLocalInfo()
                    if(data){
                        sendResponse({type: 'FROM_BROWSER', code: 0, msg: 'success', data:{ 
                            uid: parseInt(localStorage.getItem('uid')),
                            sid: localStorage.getItem('sid'),
                            version: FxVpn.constants.browserVersion,
                            intarnetIp: data.local_ip,
                            macAddress: data.mac_addr
                        }});
                        localStorage.setItem('intarnetIp',data.local_ip)
                    }
                    else {
                        sendResponse({type: 'FROM_BROWSER', code: 0, msg: 'success', data: null})
                    }
                    return true
                }
            });
        },
        // 从tabs拉取刷新页面，解决extension头像不显示的问题
        tabsDetachedListener:function(){
            chrome.tabs.onDetached.addListener(function(tabId,detachInfo){
                chrome.tabs.reload(tabId)
            })
        },
        // 退出浏览器监听器
        closeBrowserListener:function(){
            window.addEventListener("unload", function(event) {
                FxVpn.quit(JSON.stringify({
                    url: FxVpn.constants.apiHost + '/enterprise/v1/logout',
                    type: 'PC',
                    sid: localStorage.getItem('sid'),
                    unique_identification: FxVpn.constants.browserVersion,
                    intranet_ip: localStorage.getItem('intarnetIp')
                }))
                localStorage.clear()

            });
        },
        // 登录态心态检测
        loginStatusHeartBeat:function(){
            let interval = setInterval(() => {
                FxVpn.link.getSession(async (result)=>{
                    if(result && result.code === 401){
                        FxVpn.common.logoutAjax(() => {
                            localStorage.clear()
                        })
                        for(let i = 0; i< 3 ;i++){
                            let flag = await FxVpn.sslocal.disconnect('logout') // 断开sslocal
                            if(flag){
                                break
                            }
                        }
                        chrome.browserAction.setIcon({path:"../assets/vpn_icon_unlogin.png"})
                        FxVpn.proxy.switch('off')
                        clearInterval(interval)
                    }
                })
            }, 1000 * 60 * 2);
        }
    },
    /**
     *  链路相关
     */
    link:{
        getNodes: (callback = null) => {
            return new Promise((resolve,reject) => {
                if(!localStorage.getItem('uid')){
                    return 
                }
        
                $.ajax({
                    url: FxVpn.constants.apiHost + '/enterprise/v1/nodes',
                    async: true,
                    cache: false,
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        uid: parseInt(localStorage.getItem('uid')),
                        type: "PC",
                        asb: 1,
                        pro_belong: 0,
                        company_certificate: 1
                    }),
                    success: result => {
                        callback && callback.call(this,result)
                    }
                })
            })
        },
        getSession: (callback = null) => { 
            return new Promise((resolve,reject) => {
                if(!localStorage.getItem('uid') || !localStorage.getItem('sid')){
                    return
                }
                $.ajax({
                    url: FxVpn.constants.apiHost + '/enterprise/v1/session',
                    async: true,
                    cache: false,
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        uid: parseInt(localStorage.getItem('uid')),
                        sid: localStorage.getItem('sid'),
                        version: FxVpn.constants.browserVersion
                    }),
                    success: result => {
                        callback && callback.call(this,result)
                    }
                })
            })
        }
    },
    dataReport:{
        reportAjax:function(params){
            $.ajax({
                url: FxVpn.constants.dataReportUrl,
                cache: false,
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(params),
                success: function(result){
                },
                error: function(error){
                }
            })
        },
        urlWatchListener:function(){
            chrome.tabs.onUpdated.addListener(function(tabId,changeInfo,tab){
                FxVpn.common.loginStatusWatching(tab.id,tab.url)

                if(localStorage.getItem('uid') && FxVpn.constants.currentCompanyId && localStorage.getItem('phone')){
                    if(tab && 
                    tab.status === "complete" && 
                    !tab.url.startsWith('chrome:') &&
                    !tab.url.startsWith('chrome-extension:')){

                        let params = [{
                            common:{
                                "_appid": "com.fxbrowser.desktop",
                                "_appv": FxVpn.constants.browserVersion,
                                "_c": "",
                                "_m": "",
                                "_msys": "Windows",
                                "_mv": "",
                                "_n": "",
                                "_p": "product", // 正式环境 product，测试环境 develop
                                "_pv": "",
                                "_uc": "",
                                "_uuid": ""
                            },
                            event:'query_url',
                            ts: Math.round(new Date().getTime()/1000).toString(),
                            attr: {
                                uid: localStorage.getItem('uid'),
                                cid: FxVpn.constants.currentCompanyId,
                                phone: localStorage.getItem('phone'),
                                url: tab.url
                            }
                        }]   
                        FxVpn.dataReport.reportAjax(params)
                    }
                }
            })
        }
    },
    init: function() {
        localStorage.clear()
        chrome.browserAction.setIcon({path:"../assets/vpn_icon_unlogin.png"})
        
        this.proxy.init()
        this.sslocal.init()

        // 全局登录提示
        this.listeners.messageListener()
        this.listeners.closeBrowserListener()
        this.listeners.tabsDetachedListener()
        this.listeners.loginStatusHeartBeat()
        // this.listeners.tabsCreatedListener()
        this.dataReport.urlWatchListener()
    },
    quit: async function(params){
        await this.sslocal.quit(params)
    }
}


FxVpn.init()

