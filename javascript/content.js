var ContentScript = {
    injectExtensionId:function(){
        var extensionIdDom = document.createElement('input')
        // 对extensionid进行加密
        extensionIdDom.setAttribute('value',chrome.runtime.id)
        extensionIdDom.setAttribute('id','vpn-panel-id-content-string')
        extensionIdDom.style.display = 'none'
        document.body.appendChild(extensionIdDom)
    },
    listeners:{
        logoutListener:function(){
            chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
                if(request.type === 'FROM_BACKGROUND_LOGOUT'){
                    localStorage.removeItem('sid')
                }
                sendResponse('success')
            })
        }
    },
    init: function(){
        ContentScript.injectExtensionId()
        ContentScript.listeners.logoutListener()
    }
}

ContentScript.init()
